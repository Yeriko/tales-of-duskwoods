﻿using UnityEngine;
using System.Collections;

public class AceBehaviour : MonoBehaviour
{
	Rigidbody rb;
	Vector3 mov;
	bool isMoving;
	Animator anim;
	[HideInInspector]							public bool canMove; //Variable que se usara para poder hacer uso del movimiento
	[Tooltip("Velocidad del movimiento")]		public float speed = 4f; //Velocidad del personaje


	// Use this for initialization
	void Start ()
	{
		//anim = transform.GetChild(0).GetComponent<Animator> ();
		anim = GetComponentInChildren<Animator> ();
		canMove = true;
		rb = GetComponent<Rigidbody> ();		
		isMoving = false;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		Movement ();
		Animation ();
		Rotation ();

	}

	void Movement(){
		if (GameManager.sharedInstance.aceActive == true && canMove == true) {
			mov = new Vector3 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"), 0);
			rb.MovePosition (rb.position + mov * speed * Time.deltaTime);
			//transform.rotation = Quaternion.LookRotation(mov, new Vector3 (0,180,0));
			//transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(mov), 0.15F);
			if (mov != Vector3.zero) {
				isMoving = true;
			} else
				isMoving = false;

		} else {
			mov = Vector3.zero;
			isMoving = false;
		}
	}

	void Animation(){
		if (isMoving == true){
			anim.SetBool ("isWalking", true);
	}else			
		anim.SetBool ("isWalking", false);
	}

	void Rotation(){
		if (Input.GetButtonDown("Horizontal")) {
			transform.eulerAngles = (new Vector3(0, Input.GetAxisRaw("Horizontal") * 90, 0));
		}
		if (Input.GetButtonDown("Vertical")) {
			transform.eulerAngles = (new Vector3(0, 270+(Input.GetAxisRaw("Vertical") * 90), 0));
		}
	}
}

