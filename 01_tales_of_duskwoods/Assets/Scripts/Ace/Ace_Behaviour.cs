﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Ace_Behaviour : MonoBehaviour {
	public static Ace_Behaviour sharedInstance;
	Animator anim;
	Aura aura; //El aura se debera agregar como hijo (1)
	Rigidbody2D rb;
	Vector2 mov;
	CircleCollider2D attackCollider; //Hitbox del ataque
	float timebutton = 0f; //Variable que captura el tiempo que se deja presionado el ataque (Se usa para el slash attack)
	[Tooltip("Mapa de inicio")] 				public GameObject currentMap;
	[Tooltip("Prefab del ataque swordSlash")]	public GameObject swordSlash;
	[Tooltip("Velocidad del movimiento")]		public float speed = 4f; //Velocidad del personaje
	[HideInInspector]							public bool canMove; //Variable que se usara para poder hacer uso del movimiento
	[HideInInspector]							public bool canAttack; //Variable que se usara para poder hacer uso del ataque



	void Awake(){
		sharedInstance = this;
		Assert.IsNotNull (currentMap);
		Assert.IsNotNull (swordSlash);
	}


	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		rb = GetComponent<Rigidbody2D> ();		
		Camera.main.GetComponent<MainCamera> ().SetBound (currentMap);
		attackCollider = transform.GetChild (0).GetComponent<CircleCollider2D> ();
		attackCollider.enabled = false;
		canMove = true;
		canAttack = true;
		aura = transform.GetChild (1).GetComponent<Aura> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.sharedInstance.aceActive == true){
			MovePrevent (); //Verificamos si se puede mover
			Movement (); //Movemos al personaje
			Animation (); //Activamos animación
			SwordAttack (); //Revisamos ataque
			SlashAttack (); //Revisamos slash attack
		}
	}

	void FixedUpdate(){
		rb.MovePosition (rb.position + mov * speed * Time.deltaTime);
	}

	void Movement(){
		if (GameManager.sharedInstance.aceActive == true && canMove == true) {
			mov = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
		} else
			mov = Vector2.zero;
	}

	void Animation(){
		if (anim.GetBool("isDead") == true){
			anim.Play("PlayerDeath");}

		if (mov != Vector2.zero && anim.GetBool("isDead") == false) {
			anim.SetBool ("isWalking", true);
			anim.SetFloat ("movX", mov.x);
			anim.SetFloat ("movY", mov.y);
		} else {
			anim.SetBool ("isWalking", false);
		}
				}

	void SwordAttack(){
		//Detectamos el estado del animador
		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
		bool attacking = stateInfo.IsName ("Player_Attack");

		//Detectamos ataque
		if (Input.GetButtonDown("Attack") && !attacking && canAttack == true){
			anim.SetTrigger ("isAttacking");
		}
			
		//Actualización del hitbox de ataque se ponen valores con numeros magicos especificos del offset
		if(mov != Vector2.zero){
			if (mov.y == -1)
				attackCollider.offset = new Vector2 (0.43f, -0.18f);
			if(mov.y== 1)
				attackCollider.offset = new Vector2 (0.35f, 0.91f);
			if (mov.x != 0)
				attackCollider.offset = new Vector2 (mov.x/2, 0.5f);
		}

		//Controlar el collider hitbox
		if (attacking){
			float playbackTime = stateInfo.normalizedTime;
			if (playbackTime > 0.45 && playbackTime < 0.70)
				attackCollider.enabled = true;
			else
				attackCollider.enabled = false;
			}
		}
		

	void SlashAttack(){
		//Detectamos el estado del animador
		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
		bool isCharging = stateInfo.IsName ("Player_Slash");

		//Verificamos el input del boton de ataque y aumentamos el temporizador para confirmar la barra de slash attack
		if (Input.GetButton ("Attack")) {
			timebutton = timebutton + 0.01f;
		}

		//Si se cumple la condicion y se puede atacar se inicia la animacion, y se verifica el estado del aura
		if (timebutton >= 0.25f && anim.GetBool ("isCharging") == false) {
			anim.SetTrigger ("isCharging");
			aura.AuraStart ();
			canMove = false;
		} else if (Input.GetButtonUp ("Attack")) {
			timebutton = 0;
			anim.ResetTrigger ("isCharging");

			//Verificamos si se cargo la barra
			if (aura.IsLoaded ()) {
				anim.SetTrigger ("isAttacking");
				//Conseguir la rotacion del ataque
				float angle = Mathf.Atan2 (anim.GetFloat ("movY"), anim.GetFloat ("movX")) * Mathf.Rad2Deg;
				//Creamos la instancia del slash
				GameObject slashObject = Instantiate (swordSlash, transform.position, Quaternion.AngleAxis (angle, Vector3.forward));
				//Le otorgamos el movimiento inicial
				SwordSlash slash = slashObject.GetComponent<SwordSlash> ();
				slash.mov.x = anim.GetFloat ("movX");
				slash.mov.y = anim.GetFloat ("movY");

				//Detenemos la animacion del aura, dentro y fuera de la condicion
				aura.AuraStop ();
			} else 
				aura.AuraStop ();

			//Esperar unos momentos y reactivar el movimiento
			StartCoroutine(EnableMovementafter(0.4f));
		} 
	}


	void MovePrevent ()
	{//Preveemos el movimiento solamente desactivando la bandera de canMove
		if (canMove == false) {
			mov = Vector2.zero;
		}
	}

	IEnumerator EnableMovementafter(float seconds){
		yield return new WaitForSeconds (seconds);
		canMove = true;
	}

	IEnumerator OnTriggerEnter2D(Collider2D col){
		//Reducimos HP si colisionamos con un objeto con tag "Proyectil"
		if (col.gameObject.tag == "Proyectil") {
			GameManager.sharedInstance.aceHp--;
			}

		//Verificamos si el personaje ha muerto
		if (GameManager.sharedInstance.aceHp <= 0){
			anim.SetBool ("isDead", true);
			canMove = false;
			canAttack = false;
			yield return new WaitForSeconds(0.45f);
			Destroy (gameObject);
		}

		//En caso de que sea un Warp, la variable de targetMap cambiara
		if (col.gameObject.tag == "Warp") {
			currentMap = col.GetComponent<Warp> ().targetMap; //Cambiamos el mapa para la camara

		}
	}
}

