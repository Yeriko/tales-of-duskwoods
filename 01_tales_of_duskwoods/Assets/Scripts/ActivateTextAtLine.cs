﻿using UnityEngine;
using System.Collections;

public class ActivateTextAtLine : MonoBehaviour
{
	public TextAsset theText;

	public int startLine;
	public  int endLine;
	public TextBoxManager theTextBox;
	public bool destroyWhenActivated;
	public bool waitForPress = false;
	public bool requireButtonPress;
	Collider2D col;
	// Use this for initialization
	void Start ()
	{
		theTextBox = FindObjectOfType<TextBoxManager> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (waitForPress == true && Input.GetButtonDown ("Attack") && theTextBox.isActive == false) {
			waitForPress = false;
			theTextBox.ReloadScript (theText);
			theTextBox.currentLine = startLine;
			theTextBox.endAtLine = endLine;
			theTextBox.EnableTextBox ();
			col.GetComponent<Animator>().SetBool("isWalking", false);

		}
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.tag == "Ace" || other.tag == "Jolyne") {
			col = other;
			if (requireButtonPress) {
				waitForPress = true;
				return;
			}



		}

		if (destroyWhenActivated) {
			Destroy (gameObject);
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		if (other.tag == "Ace" || other.tag == "Jolyne") {
			waitForPress = false;
		}
	}
}

