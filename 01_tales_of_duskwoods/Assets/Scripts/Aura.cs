﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aura : MonoBehaviour {

	//Tiempo de precarga
	public float waitBeforePlay;
	Animator anim;
	Coroutine manager;
	bool loaded;


	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}

	public void AuraStart(){
		manager = StartCoroutine (Manager());
		anim.Play ("AuraIdle");
	}

			public void AuraStop(){
		if (manager != null) {
			StopCoroutine (manager);
			anim.Play ("AuraIdle");
			loaded = false;
		}
	}

	//Metodo para comprobar si ya hemos cargado suficiente
	public IEnumerator Manager(){
		//yield return new WaitForSeconds (GameManager.sharedInstance.auraChargeTime);			
		anim.Play("AuraPlay");
		yield return new WaitForSeconds (GameManager.sharedInstance.auraChargeTime - 0.50f);

		loaded = true;
	}

	//Metodo para comprobar si se cargo lo suficiente
	public bool IsLoaded(){
		return loaded;
	}
}
