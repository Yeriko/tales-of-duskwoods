﻿using UnityEngine;
using System.Collections;

public class ButtonBehaviour : MonoBehaviour
{
	Animator anim;


	void Awake (){
		anim = GetComponent<Animator> ();
	}


	void OnTriggerEnter2D(Collider2D col){


		if (col.gameObject.tag == "Object" || col.gameObject.tag == "Ace" || col.gameObject.tag == "Jolyne") {
			anim.SetBool ("isActivated", true);
		} 
	}

	void OnTriggerExit2D (Collider2D col)
	{
		if (col.gameObject.tag == "Object" || col.gameObject.tag == "Ace" || col.gameObject.tag == "Jolyne") {
			anim.SetBool ("isActivated", false);
		} 

	}
}

