﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class CameraPlayer2: MonoBehaviour {
	public float smoothTime = 2f;
	public Transform target;
	float tLX, tLY, bRX, bRY;
	Vector2 velocity;
	public GameObject startMap;
	void Awake (){
		//target = GameObject.FindGameObjectWithTag ("Player").transform;
	}

	void Start (){
		//Forzar la resolucíon si no estamos en versión WEB
		if(Application.platform != RuntimePlatform.WebGLPlayer)
			Screen.SetResolution (1024, 768, true);
		transform.position = new Vector3 (target.position.x, target.position.y, transform.position.z);
		Assert.IsNotNull (startMap);
	}


	// Update is called once per frame
	void Update () {
		//startMap = Player2.sharedInstance.currentMap;
		//SetBound (Jolyne_Bejaviour.sharedInstance.currentMap);
		float posX = Mathf.Round (Mathf.SmoothDamp (transform.position.x, target.position.x, ref velocity.x, smoothTime) * 100) / 100;
		float posY = Mathf.Round (Mathf.SmoothDamp (transform.position.y, target.position.y, ref velocity.y, smoothTime) * 100) / 100;
		transform.position = new Vector3 (Mathf.Clamp (posX, tLX, bRX), Mathf.Clamp (posY, bRY, tLY), transform.position.z);



	}

	public void SetBound (GameObject map){
		Tiled2Unity.TiledMap config = map.GetComponent<Tiled2Unity.TiledMap> ();
	//float cameraSize = Camera.main.orthographicSize;
		float cameraSize = GetComponent<Camera>().orthographicSize;
		tLX = map.transform.position.x + cameraSize + 1.8f;
		tLY = map.transform.position.y - cameraSize ;
		bRX = map.transform.position.x + config.NumTilesWide - cameraSize- 1.8f	;
		bRY = map.transform.position.y - config.NumTilesHigh + cameraSize;	

		FastMove ();
	}

	public void FastMove(){
		transform.position = new Vector3 (target.position.x, target.position.y, transform.position.z);
	}


}
