﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class DungeonManager : MonoBehaviour
{
	public GameObject switch01, switch02,switch03, switch04, button01, button02;
	public GameObject door01, fireContainer1, correctButton, fakeButton, fireContainer2, switchesContainer1, lastSwitch1, lastSwitch2, rock, 
	lastButton, bossDoor, dungeonDoor, dungeonBoss;
	Animator switch01anim, switch02anim, switch03anim, switch04anim, door01anim, button01anim, button02anim,
	correctButtonAnim, fakeButtonAnim, lastSwitch1anim, lastSwitch2anim, lastButtonAnim, bossDoorAnim, dungeonDoorAnim;
	public Image imageAce, imageJolyne;
	public Text thanks;
	// Use this for initialization
	void Awake ()
	{

		//Primera puerta
		switch01anim = switch01.GetComponent<Animator> ();
		switch02anim = switch02.GetComponent<Animator> ();
		switch03anim = switch03.GetComponent<Animator> ();
		switch04anim = switch04.GetComponent<Animator> ();
		door01anim = door01.GetComponent<Animator> ();

		//Segunda puerta
		button01anim = button01.GetComponent<Animator> ();
		button02anim = button02.GetComponent<Animator> ();

		//Tercera puerta
		correctButtonAnim = correctButton.GetComponent<Animator> ();
		fakeButtonAnim = fakeButton.GetComponent<Animator> ();

		//Cuarta puerta
		lastSwitch1anim = lastSwitch1.GetComponent<Animator>();
		lastSwitch2anim = lastSwitch2.GetComponent<Animator> ();
		lastButtonAnim = lastButton.GetComponent<Animator> ();
		bossDoorAnim = bossDoor.GetComponent<Animator> ();
		dungeonDoorAnim = dungeonDoor.GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update ()
	{
		//Primera puerta
		if (switch01anim.GetBool ("isActivated") == true && switch02anim.GetBool ("isActivated") == true &&
		    switch03anim.GetBool ("isActivated") == true && switch04anim.GetBool ("isActivated") == true) {

			door01anim.SetTrigger ("isOpen");
			door01.GetComponent<BoxCollider2D> ().enabled = false;
		}

		//Segunda puerta
		if (button01anim.GetBool ("isActivated") == true && button02anim.GetBool ("isActivated") == true) {
			fireContainer1.SetActive (false);
	
		} else
			fireContainer1.SetActive (true);


		//Tercera puerta
		if (correctButtonAnim.GetBool ("isActivated") == true) {
			fireContainer2.SetActive (false);
			switchesContainer1.SetActive (true);
		}

		//Cuarta puerta
		if (lastSwitch1anim.GetBool ("isActivated") == true && lastSwitch2anim.GetBool ("isActivated") == true)
			rock.gameObject.SetActive (false);

		if(lastButtonAnim.GetBool ("isActivated") == true){
			Debug.Log ("simon");
			bossDoorAnim.SetTrigger ("isOpen");
			bossDoor.GetComponent<BoxCollider2D> ().enabled = false;

		}

		//Boss
		if (dungeonBoss == null && GameManager.sharedInstance.aceActive == true) {
			Time.timeScale = 0;
			imageAce.enabled = true;
			thanks.enabled = true;
		}
		if (dungeonBoss == null && GameManager.sharedInstance.jolyneActive == true) {
			Time.timeScale = 0;
			imageJolyne.enabled = true;
			thanks.enabled = true;

		}
		
	}
}

