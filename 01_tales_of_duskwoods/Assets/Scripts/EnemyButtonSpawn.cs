﻿using UnityEngine;
using System.Collections;

public class EnemyButtonSpawn : MonoBehaviour
{
	Animator anim;

	void Awake(){
		anim = GetComponent<Animator>();
	}

	void Update(){
		if (anim.GetBool ("isActivated") == true)
			transform.GetChild (0).gameObject.SetActive (true);
	}
	void OnTriggerEnter2D (Collider2D col){
		if (col.gameObject.tag == "Ace" || col.gameObject.tag == "Jolyne")
			anim.SetBool ("isActivated", true);

	
	}
}

