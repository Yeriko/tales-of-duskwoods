﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyLog : MonoBehaviour {
	float distance;
	//Destruccion variables
	public string destroyState;
	//public float timeForDisable;


	//Variables para gestionar el rango de vision
	public float visionRadius;
	public float attackRadius;
	public float speed;
	float distanceAce, distanceJolyne;
	Vector3 dir;

	//Variable para guardar info del jugador
	public GameObject playerAce, playerJolyne;
	public Vector3 initialPosition, target;

	//Variables relacionadas con el ataque
	[Tooltip("Prefab del proyectil a disparar")]	public GameObject proyectilPrefab;
	[Tooltip("Velocidad del ataque en segundos")]	public float attackSpeed = 2f;
	bool isAttacking;

	//Variables relacionadas con la vida
	[Tooltip("Puntos máximos de vida")]
	public float maxHP =3;
	[Tooltip("Vida actual")]
	public float hp;
	Vector3 porcentajeBarra; //Almacena el espacio de la barra de vida
	float maxWidhtlifebar;	 //Ayuda a calcular la formula para poder sacar un porcentaje de la barra

	//Animador y cuerpos cinematico con la rotación en Z congelada
	Animator anim;
	Rigidbody2D rb2d;
	bool canMove;


	//----------------------------------------------------------------------------------------------------------------------
	public void Start () {
		distance = 0;
		//Inicializazamos variables
		hp = maxHP;
		canMove = false;
		initialPosition = transform.position;
		anim = GetComponent<Animator> ();
		rb2d = GetComponent<Rigidbody2D> ();

		//Recuperamos al jugador gracias al tag
		playerJolyne = GameObject.FindGameObjectWithTag ("Jolyne");
		playerAce = GameObject.FindGameObjectWithTag ("Ace");
		 
		//Recuperamos el hijo 0 que es la barra de vida
		porcentajeBarra = transform.GetChild(0).GetChild (0).localScale;
		maxWidhtlifebar = transform.GetChild (0).GetChild (0).localScale.x;
	}


	//*******************************************************************************
	public void Update () {
		
		ChangeTarget ();
		RaycastMethod ();
		WakeAndAttack ();








	}

	//Dibujar radio de visión
	void OnDrawGizmosSelected(){
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere (transform.position, visionRadius);
		Gizmos.DrawWireSphere (transform.position, attackRadius);
	}

	IEnumerator Attack(float seconds){
		isAttacking = true; //Activamos la bandera
		//Creamos la roca
		if (target != initialPosition&& proyectilPrefab != null){
			Instantiate (proyectilPrefab, transform.position, transform.rotation);
			//Esperamos para repetir el ataque
			yield return new WaitForSeconds(seconds);
		}
		isAttacking = false;
	}
		
	void OnGUI(){
	//Guardamos la posición del enemigo respecto a la camara
		if (GameManager.sharedInstance.aceActive == true) {
			Vector2 pos = Camera.main.WorldToScreenPoint (transform.position);
		}
			}

	IEnumerator OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Attack") {
			hp -= 1.0f;	
			porcentajeBarra.x = ((hp) * maxWidhtlifebar) / maxHP;
			transform.GetChild (0).GetChild (0).localScale = porcentajeBarra;

			if (hp <= 0) {
				anim.SetBool ("isDead", true);
				canMove = false;
				Destroy (transform.GetChild (0).gameObject);
			
				yield return new WaitForSeconds (0.45f);

				Destroy (gameObject);
			}
		}
	}

		
	void ChangeTarget (){
		//Calculamos la distancia y direeción actual hasta el target
		distanceAce = Vector3.Distance(playerAce.transform.position, transform.position);
		distanceJolyne = Vector3.Distance (playerJolyne.transform.position, transform.position);
		if (distanceAce < distanceJolyne) {
			target = playerAce.transform.position;
			distance = distanceAce;
		} else if (distanceJolyne < distanceAce) {
			target = playerJolyne.transform.position;
			distance = distanceJolyne;
		} 
		dir = (target - transform.position).normalized;	
	}

	void WakeAndAttack(){
		//Si el enemigo esta en ragno de ataque nos levantamos y le atacamos
		if (canMove == true) {
			if (target != initialPosition && distance < attackRadius) {
				//Aqui le atacamos pero por ahora simplemente cambiamos la animación
				anim.SetFloat ("movX", dir.x);
				anim.SetFloat ("movY", dir.y);
				anim.Play ("LogWalking", -1, 0); //Congela la animación al andar

				//Atacar
				if (!isAttacking)
					StartCoroutine (Attack (attackSpeed));

			} else {//Nos movemos hacia el
				rb2d.MovePosition (transform.position + dir * speed * Time.deltaTime);
				//Al movernos establecemos la animación de movimiento
				anim.speed = 1;
				anim.SetFloat ("movX", dir.x);
				anim.SetFloat ("movY", dir.y);
				anim.SetBool ("isWalking", true);
			}

			//SE MUERE
			if (anim.GetBool("isDead") == true && canMove == false){
				anim.Play("CharDeath");}

			//Una ultima comprobacion para evitar bugs forzando la posicion inicial
			if (target == initialPosition && distance < 0.03f){
				transform.position = initialPosition;
				//Cambiamos la animación de nuevo a Idle
				anim.SetBool("isWalking", false); 
			}


			
		}
	}

	void RaycastMethod(){
		//Comprobamos un raycast del enemigo al jugador
		if (distance < visionRadius) {
			canMove = true;

		} else {
			target = initialPosition;
			distance = Vector3.Distance(initialPosition, transform.position);
			dir = (target - transform.position).normalized;	
		}
		Vector3 forwardAce = transform.TransformDirection(playerAce.transform.position - transform.position);
		Vector3 forwardJolyne = transform.TransformDirection(playerJolyne.transform.position - transform.position);

		Debug.DrawRay (transform.position, forwardAce, Color.red);
		Debug.DrawRay (transform.position, forwardJolyne, Color.blue);


		//Debug operativo de linea hasta el target
		Debug.DrawLine(transform.position, target, Color.green);
	}

	}


