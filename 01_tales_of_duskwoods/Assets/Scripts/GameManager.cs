﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {
	public Transform pauseCanvas;
	public static GameManager sharedInstance;
	public  int aceHp, jolyneHp;
	public int aceMaxHp, jolyneMaxHp; 
	public float auraChargeTime;
	public bool aceActive, jolyneActive, isPaused, canChange = true;
	public KeyCode placeholder = KeyCode.Z;
	public bool onText = false, canPause = true;
	public GameObject Ace, Jolyne;

	void Awake (){
		isPaused = false;
		sharedInstance = this;
		aceMaxHp = 10;
		jolyneMaxHp = 10;
		auraChargeTime = 1.0f;
	}
	// Use this for initialization
	void Start () {
		aceHp = aceMaxHp;
		jolyneHp = jolyneMaxHp;
		aceActive = true;
		jolyneActive = false;
	}
	
	// Update is called once per frame
	void  Update () {
		if (Input.GetButtonDown ("Pause") && onText == false) {
			Pause ();
		}
		PlayerChange ();


	}




	public void PlayerChange(){

		if(Input.GetButtonDown("ChangeChar") && canChange == true){			
			aceActive = !aceActive;
			jolyneActive = !jolyneActive;
			}

	
		}

	public void Pause ()
	{
		if (pauseCanvas.gameObject.activeInHierarchy == false && canPause == true) {
			isPaused = true;
			pauseCanvas.gameObject.SetActive (true);
			canChange = false;
			Time.timeScale = 0;
			DisablePlayers ();


		} else if (canPause == true){
			pauseCanvas.gameObject.SetActive (false);
			Time.timeScale = 1;
			isPaused = false;
			canChange = true;
			EnablePlayers ();
		}
	}

	public void ResetLevel(){
		Pause ();
		SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);

	}

	public void GoToMainMenu(){
		Pause ();
		SceneManager.LoadScene("MainMenu");
	}

	public void Quit(){
		Application.Quit();
	}

	public void DisablePlayers(){
		GameObject.FindGameObjectWithTag ("Ace").GetComponent<Ace_Behaviour> ().enabled = false;
		GameObject.FindGameObjectWithTag ("Jolyne").GetComponent<Jolyne_Bejaviour> ().enabled = false;
		canChange = false; 
	}


	public void EnablePlayers(){
		GameObject.FindGameObjectWithTag ("Ace").GetComponent<Ace_Behaviour> ().enabled = true;
		GameObject.FindGameObjectWithTag ("Jolyne").GetComponent<Jolyne_Bejaviour> ().enabled = true;
		canChange = true;
	}

}

	
	

