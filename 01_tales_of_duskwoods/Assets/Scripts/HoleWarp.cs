﻿using UnityEngine;
using System.Collections;

public class HoleWarp : MonoBehaviour
{
	bool canWarp = true;




	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "Ace" || other.gameObject.tag == "Jolyne" && canWarp) {
			other.transform.position = transform.GetChild (0).transform.position;
			canWarp = false;
		} 
	}
}		