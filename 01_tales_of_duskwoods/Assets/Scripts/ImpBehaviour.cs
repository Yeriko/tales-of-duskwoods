﻿using UnityEngine;
using System.Collections;

public class ImpBehaviour : MonoBehaviour
{
	Vector2 initialPosition, mov;
	public int direction = -1;
	public float minNum, maxNum;
	public float rightLimit;
	public float leftLimit;
	public float speed = 4.0f;
	bool isAttacking, canDamage;
	Rigidbody2D rb;
	[Tooltip("Velocidad del ataque en segundos")]	public float attackSpeed = 3f;
	[Tooltip("Prefab del proyectil a disparar")]	public GameObject proyectilPrefab;

	//Variables relacionadas con la vida
	[Tooltip("Puntos máximos de vida")]
	public float maxHP =3;
	[Tooltip("Vida actual")]
	public float hp;
	Vector3 porcentajeBarra; //Almacena el espacio de la barra de vida
	float maxWidhtlifebar;	 //Ayuda a calcular la formula para poder sacar un porcentaje de la barra


	void Start ()
	{
		//Recuperamos el hijo 0 que es la barra de vida
		porcentajeBarra = transform.GetChild(0).GetChild (0).localScale;
		maxWidhtlifebar = transform.GetChild (0).GetChild (0).localScale.x;
		initialPosition = transform.position;
		rb = GetComponent<Rigidbody2D> ();	
		isAttacking = false;
		canDamage = true;
		hp = maxHP;

	}

	void FixedUpdate (){
		rb.MovePosition (rb.position + mov * speed * Time.deltaTime);
		}

	void Update (){
		mov = new Vector2 (speed * direction * Time.deltaTime, 0);
		if (transform.position.x < initialPosition.x - leftLimit) {
			direction = 1;
		} else if (transform.position.x > initialPosition.x + rightLimit){
			direction = -1;
		}

		//////////////////////////////
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 10.0f);
		if (hit.collider.gameObject.tag == "Ace" || hit.collider.gameObject.tag == "Jolyne")
		if (!isAttacking) {
			StartCoroutine (Attack (attackSpeed));
			Debug.Log ("Aqui");}
			


	}

	IEnumerator Attack(float seconds){
		isAttacking = true; //Activamos la bandera
		//Creamos la roca
		//if (target != initialPosition&& proyectilPrefab != null){
			Instantiate (proyectilPrefab, transform.position, transform.rotation);
			//Esperamos para repetir el ataque
			yield return new WaitForSeconds(seconds);
		//}
		isAttacking = false;
	}

	IEnumerator OnTriggerEnter2D (Collider2D col)
	{
		
		if (col.tag == "Ace" && canDamage == true) {
			GameManager.sharedInstance.aceHp--;
			canDamage = false;
			yield return new WaitForSeconds (0.5f);
			canDamage = true;
		}
		if (col.gameObject.tag == "Jolyne" && canDamage == true) {
			GameManager.sharedInstance.jolyneHp--;
			canDamage = false;
			yield return new WaitForSeconds (0.5f);
			canDamage = true;
		}


		if (col.gameObject.tag == "Attack") {
			hp -= 1.0f;	
			porcentajeBarra.x = ((hp) * maxWidhtlifebar) / maxHP;
			transform.GetChild (0).GetChild (0).localScale = porcentajeBarra;

			if (hp <= 0) {
				Destroy (transform.GetChild (0).gameObject);
				yield return new WaitForSeconds (0.45f);
				Destroy (gameObject);
			}
		}
	}
}
	