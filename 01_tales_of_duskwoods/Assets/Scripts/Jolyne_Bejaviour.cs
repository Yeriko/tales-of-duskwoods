﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Jolyne_Bejaviour : MonoBehaviour {
	public static Jolyne_Bejaviour sharedInstance;
	public float speed = 4f;
	Animator anim;
	Rigidbody2D rb;
	Vector2 mov;
	[Tooltip("Prefab del ataque swordSlash")]	public GameObject arrowProyectile;
	public  GameObject currentMap;
	//public GameObject swordSlash;
	//CircleCollider2D attackCollider;
	public bool canMove;
	bool canAttack;
	//Aura aura;
	//public Camera cam;
	public float chargeTimer = 0;

	void Awake(){
		canAttack = true;
		Assert.IsNotNull (currentMap);
		sharedInstance = this;
		//Assert.IsNotNull (swordSlash);
	}


	// Use this for initialization
	void Start () {
		
		//cam.GetComponent<Camera> ();
		anim = GetComponent<Animator> ();
		rb = GetComponent<Rigidbody2D> ();
		//attackCollider = transform.GetChild (0).GetComponent<CircleCollider2D> ();
		//attackCollider.enabled = false;
		canMove = true;
		//canAttack = true;
		//aura = transform.GetChild (1).GetComponent<Aura> ();
	//	Camera.main.GetComponent<MainCamera> ().SetBound (currentMap);
	}

	// Update is called once per frame
	void Update () {
		currentMap = sharedInstance.currentMap;
		if (GameManager.sharedInstance.jolyneActive == true ) {

			if (anim.GetBool ("isCharging") == true)
				chargeTimer = chargeTimer + 0.01f;
			MovePrevent ();
			BowAttack ();
			Movement ();
			Animation ();

			//SlashAttack ();
		}
	}

	void FixedUpdate(){
		if(canMove)
		rb.MovePosition (rb.position + mov * speed * Time.deltaTime);
	}

	void Movement(){
		if (GameManager.sharedInstance.jolyneActive == true && canMove == true && GameManager.sharedInstance.isPaused == false) {
			mov = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical")); 
		} else
			mov = Vector2.zero;;
	}

	void Animation(){
		if (GameManager.sharedInstance.isPaused == false) {
			if (anim.GetBool ("isDead") == true) {
				anim.Play ("JolyneDeath");
			}

			if (mov != Vector2.zero && anim.GetBool ("isDead") == false) {
				anim.SetBool ("isWalking", true);
				anim.SetFloat ("movX", mov.x);
				anim.SetFloat ("movY", mov.y);
			} else {
				anim.SetBool ("isWalking", false);
			}
		}
	}

	void BowAttack(){
		//Detectamos el estado del animador
		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
		bool attacking = stateInfo.IsName ("Player_Attack");

		//Detectamos ataque
		if (Input.GetButtonDown("Attack") && canAttack == true && anim.GetBool ("isDead") == false){
			anim.SetBool ("isCharging", true);
			canMove = false;
			anim.SetBool ("isCharged", false);
			chargeTimer = 0;
		}

		if (Input.GetButtonUp("Attack") && anim.GetBool("isDead") == false){
			if (chargeTimer > 0.25) {
				anim.SetBool ("isCharged", true);
				anim.SetBool ("isCharging", false);
				canMove = true;
	
				/////////////////////////////////////////////////
				float angle = Mathf.Atan2 (anim.GetFloat ("movY"), anim.GetFloat ("movX")) * Mathf.Rad2Deg;
				GameObject arrow = Instantiate (arrowProyectile, transform.position, Quaternion.AngleAxis (angle, Vector3.forward));
				ArrowProyectile proyectile = arrow.GetComponent<ArrowProyectile> ();
				proyectile.mov.x = anim.GetFloat ("movX");
				proyectile.mov.y = anim.GetFloat ("movY");


				//////////////////////////






			} else if (chargeTimer < 0.25) {
				anim.SetBool ("isCharging", false);
				canMove = true;
			}
		


		}
	
		}

	/*
	void SlashAttack(){
		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
		bool isCharging = stateInfo.IsName ("Player_Slash");

		if (Input.GetKeyDown (KeyCode.M)) {
			anim.SetTrigger ("isCharging");
			aura.AuraStart ();
		} else if (Input.GetKeyUp (KeyCode.M)) {
			anim.SetTrigger ("isAttacking");
			if (aura.IsLoaded ()) {

				//Conseguir la rotacion del ataque
				float angle = Mathf.Atan2 (anim.GetFloat ("movY"), anim.GetFloat ("movX")) * Mathf.Rad2Deg;

				//Creamos la instancia del slash
				GameObject slashObject = Instantiate (swordSlash, transform.position, Quaternion.AngleAxis (angle, Vector3.forward));

				//Le otorgamos el movimiento inicial
				SwordSlash slash = slashObject.GetComponent<SwordSlash> ();
				slash.mov.x = anim.GetFloat ("movX");
				slash.mov.y = anim.GetFloat ("movY");

			}
			aura.AuraStop ();
			//Esperar unos momentos y reactivar el movimiento
			StartCoroutine(EnableMovementafter(0.4f));
		} 

		//Prevenimos movimiento mientras cargamos
		if (isCharging) {
			canMove = false;
		}
	}
*/

	void MovePrevent ()
	{
		if (canMove == false || anim.GetBool("isCharging") == true || GameManager.sharedInstance.isPaused == true) {
			mov = Vector2.zero;
		}
	}

	IEnumerator EnableMovementafter(float seconds){
		yield return new WaitForSeconds (seconds);
		canMove = true;
		anim.SetBool ("isCharged", false);
	}
	//IEnumerator 
	IEnumerator OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Proyectil" && anim.GetBool("isDead") == false) {
			GameManager.sharedInstance.jolyneHp--;
			if (GameManager.sharedInstance.jolyneHp <= 0){
				anim.SetBool ("isDead", true);
				canMove = false;
				canAttack = false;

			}
		}
		if (col.gameObject.tag == "Warp") {
			canMove = false;
			yield return new WaitForSeconds (1);
			currentMap = col.GetComponent<Warp> ().targetMap; //Cambiamos el mapa para la camara
			canMove= true;
		}
	}





}

