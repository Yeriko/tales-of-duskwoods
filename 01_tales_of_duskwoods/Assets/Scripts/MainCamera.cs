﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera: MonoBehaviour {
	public float smoothTime = 2f;
	public Transform target, ace, jolyne;
	float tLX, tLY, bRX, bRY;
	Vector2 velocity;
	bool playerActive; // True Ace, False Jolyne

	void Awake (){
		target = ace;
		playerActive = true;
		//target = GameObject.FindGameObjectWithTag ("Player").transform;
	}

	void Start (){
		//Forzar la resolucíon si no estamos en versión WEB
		if(Application.platform != RuntimePlatform.WebGLPlayer)
			Screen.SetResolution (1024, 768, true);

	}


	// Update is called once per frame
	void Update () {
		CameraSwap ();
		LimitCamera ();
	}

	void LimitCamera(){
		float posX = Mathf.Round (Mathf.SmoothDamp (transform.position.x, target.position.x, ref velocity.x, smoothTime) * 100) / 100;
		float posY = Mathf.Round (Mathf.SmoothDamp (transform.position.y, target.position.y, ref velocity.y, smoothTime) * 100) / 100;
		transform.position = new Vector3 (Mathf.Clamp (posX, tLX, bRX), Mathf.Clamp (posY, bRY, tLY), transform.position.z);

	}

	public void SetBound (GameObject map){
		Tiled2Unity.TiledMap config = map.GetComponent<Tiled2Unity.TiledMap> ();
		float cameraSize = Camera.main.orthographicSize;


		tLX = map.transform.position.x + cameraSize + 1.8f;
		tLY = map.transform.position.y - cameraSize ;
		bRX = map.transform.position.x + config.NumTilesWide - cameraSize- 1.8f	;
		bRY = map.transform.position.y - config.NumTilesHigh + cameraSize;	

		FastMove ();
	}

	public void FastMove(){
		transform.position = new Vector3 (target.position.x, target.position.y, transform.position.z);
	}

	void CameraSwap (){
		if (Input.GetButtonDown ("ChangeChar") && GameManager.sharedInstance.canChange == true) {	
			playerActive = !playerActive;
			if (playerActive == true) {
				target = ace;
				SetBound (Ace_Behaviour.sharedInstance.currentMap);

				Debug.Log ("Ace");
			}
			else 		if (Input.GetButtonDown ("ChangeChar") && GameManager.sharedInstance.canChange == true) {	 
				target = jolyne;
				SetBound (Jolyne_Bejaviour.sharedInstance.currentMap);
				Debug.Log ("Jolyne");
			}
		}
			
	}
}
