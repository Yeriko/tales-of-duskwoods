﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	[Tooltip("Flecha")] 				public GameObject arrow, typeOfController;
	[Tooltip("Menú principal")] 		public GameObject mainMenu;
	[Tooltip("Menú de opciones")]		public GameObject optionMenu;
	Text controllerText;
	int index, numController = 1;
	bool isMenu = true, isOption = false; //Variables para ver que parte del menu esta activo

	// Use this for initialization
		void Awake () {
			Dibujar ();
		controllerText = typeOfController.GetComponent<Text> (); 
		controllerText.text = "Left Handed";	
	}

	
	// Update is called once per frame
	void Update () {
		MenuBehaviour ();
	

	}



	void MenuBehaviour (){
		//Variables para el control de la flecha, con los botones arriba y abajo
			bool up = Input.GetKeyDown ("up");
			bool down = Input.GetKeyDown ("down");
			if (up) index--;
			if (down)index++;
					
	if (Input.GetKeyDown ("return"))
		Accion ();

		//Maximos y minimos de los menus para que la flecha se cicle
		if (isMenu == true) {
			if (index > mainMenu.transform.childCount - 1)
				index = 0;
			else if (index < 0)
				index = mainMenu.transform.childCount - 1;
		}

		if (isOption == true) {
			if (index > optionMenu.transform.childCount - 1)
				index = 0;
			else if (index < 0)
				index = optionMenu.transform.childCount - 1;
		}
		if (up || down)
			Dibujar ();
		}

	void Dibujar(){
		//Movimiento de la flecha, toma el valor de la opcion y se dibuja en el pivote
		if(isMenu){
			Transform option = mainMenu.transform.GetChild (index);
			arrow.transform.position = option.position;
		}
		if (isOption){
			Transform option = optionMenu.transform.GetChild (index);
			arrow.transform.position = option.position;
			Debug.Log (index);
		}
	}


void Accion (){
		//Menu principal
		if (isMenu == true) {
			Transform option = mainMenu.transform.GetChild (index);
			switch (index) {
			case 0: //Jugar
				SceneManager.LoadScene ("Town and city");
				break;
			case 1: //Continuar
				break;
			case 2: //Opciones
				index = 99;
				mainMenu.SetActive (false);
				isMenu = false;
				optionMenu.SetActive (true);
				isOption = true;
				Dibujar ();
				break;
			case 3: //Salir
				Application.Quit ();
				break;
			}
		}

		//Menu opciones
		if (isOption == true && isMenu == false) {
			Transform option = optionMenu.transform.GetChild (index);
			switch (index) {
			case 99:
				index = 0;
				break;
			case 0://Cambiar control
				ChangeController();
				break;
			case 1://Regresar
				mainMenu.SetActive (true);
				isMenu = true;
				optionMenu.SetActive (false);
				isOption = false;
				index = 2;
				Dibujar ();
				break;
			}
		}


	}

	void ChangeController (){			
		numController++;
		if (numController > 3)
			numController = 1;

		switch (numController) {
		case 1:
			controllerText.text = "Left handed";
			break;
		case 2:
			controllerText.text = "Right handed";
			break;
		case 3:
			controllerText.text = "Two handed";
			break;

		}

	}

	public void PatreonLink(){
		Application.OpenURL("https://www.patreon.com/wildhogstudio");
	}

}