﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigBehaviour : MonoBehaviour {
	Animator animar;
	bool Startposition = false;
	int numanimation;
	int xAxis;
	int yAxis;
	// Use this for initialization
	void Start () {
		animar = GetComponent<Animator> ();	
		numanimation = Random.Range (0, 4);
		switch (numanimation) {
		case 0:
			xAxis = 0;
			yAxis = 1;
			break;
		case 1:
			xAxis = 1;
			yAxis = 0;
			break;
		case 2:
			xAxis = 0;
			yAxis = -1;
			break;
		case 3:
			xAxis = -1;
			yAxis = 0;
			break;

		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Startposition == false){
			animar.SetFloat("movX", xAxis);
			animar.SetFloat("movY", yAxis);

			Startposition = true;
		}
	}
}
