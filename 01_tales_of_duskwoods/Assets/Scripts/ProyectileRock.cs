﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectileRock : MonoBehaviour {
	[Tooltip("Velocidad de movimiento en unidades del mundo")]
	public float speed;

	GameObject playerJolyne, playerAce; //Recuperamos al objeto jugador
	Rigidbody2D rb2d; //Recuperamos el rigidbody
	Vector3 target, dir; //Vectores para almacenar el objetivo y su dirección


	// Use this for initialization
	void Start () {

		//Recuperamos al jugador gracias al tag
		playerJolyne = GameObject.FindGameObjectWithTag ("Jolyne");
		playerAce = GameObject.FindGameObjectWithTag ("Ace");
		rb2d = GetComponent<Rigidbody2D> ();


		//*************************
		float distanceAce = Vector3.Distance(playerAce.transform.position, transform.position);
		float distanceJolyne = Vector3.Distance (playerJolyne.transform.position, transform.position);

		if (distanceAce < distanceJolyne)
			target = playerAce.transform.position;
		else if (distanceJolyne < distanceAce)
			target = playerJolyne.transform.position;
		//Recuperamos la direccion del jugador y la normalizamos
		//if (player != null) {
			dir = (target - transform.position).normalized;
		//}
	}
		

	
	// Update is called once per frame
	void FixedUpdate () {
		//Si hay un objetivo movemos la roca hacia su posición
		if (target != Vector3.zero){
			rb2d.MovePosition (transform.position + (dir * speed) * Time.deltaTime);
		}
	}

	void OnTriggerEnter2D(Collider2D col){
	//Al chocar con el jugador se borra
		if (col.transform.tag == "Ace" || col.transform.tag == "Jolyne" || col.transform.tag == "Attack"){
			Destroy (gameObject);
		}
	}

	void OnBecameInvisible(){
		Destroy (gameObject);
	}
}
