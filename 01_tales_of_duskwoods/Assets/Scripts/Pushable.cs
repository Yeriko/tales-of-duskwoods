﻿using UnityEngine;
using System.Collections;

public class Pushable : MonoBehaviour
{
	public float distance=1;
	public LayerMask boxMask;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		Physics2D.queriesStartInColliders = false;
		RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector2.right * transform.localScale.x, distance, boxMask);
	}

	void OnDrawGuizmos(){
		Gizmos.color = Color.red;
			//Gizmos.DrawLine (transform.position, (Vector2)transform.position, Vector2.right * transform.localScale.x, distance);
	}
}

