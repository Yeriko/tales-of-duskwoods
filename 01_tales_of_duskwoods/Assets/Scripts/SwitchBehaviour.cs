﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchBehaviour : MonoBehaviour {
	Animator anim;


	void Awake (){
		anim = GetComponent<Animator> ();
	}


	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Attack") {
			anim.SetBool ("isActivated", true);
		}
	}
}
