﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TextBoxManager : MonoBehaviour
{
	public GameObject textBox;
	public Text theText;
	public TextAsset textFile;
	public string[] textlines;
	public int currentLine;
	public int endAtLine;
	public bool isActive;
	public bool stopPlayerMovement;
	private bool isTyping = false, cancelTyping = false;
	public float typeSpeed; 
	//Hacer que el personaje no se mueva*************************************************


	// Use this for initialization
	void Start () {
		endAtLine = textlines.Length;
		if (textFile != null) {
			textlines = (textFile.text.Split ('\n'));
		}

		if (endAtLine == 0) {
			endAtLine = textlines.Length - 1;
		}

		if (isActive) {
			EnableTextBox ();
		} else {
			DisableTextBox ();
		}
	}

	void Update ()
	{
		Debug.Log (textlines.Length);
		if (!isActive) {
			return;
		}
		//theText.text = textlines [currentLine];

		if (Input.GetButtonDown ("Attack") && currentLine == endAtLine -1) {
			DisableTextBox ();
		}

		if (Input.GetButtonDown ("Attack"))  {
			if (!isTyping && currentLine < endAtLine) {
				currentLine += 1;
				if (Input.GetButtonDown ("Attack") && currentLine == textlines.Length) {
					DisableTextBox ();
					currentLine = 0;
				} else {
					StartCoroutine (TextScroll (textlines [currentLine]));
				}
		
			} else if (isTyping && !cancelTyping) {
				cancelTyping = false;////////////////////////////////////////////////
//Aqui se debe agregar el texto completo pero estoy hasta la madre **************************************************

		} 

	}
	}

	private IEnumerator  TextScroll(string lineOfText){
		int letter = 0;
		theText.text = "";
		isTyping = true;
		cancelTyping = false;

		while (isTyping && !cancelTyping && letter < lineOfText.Length -1){
			theText.text += lineOfText [letter];
			letter += 1;
			yield return new WaitForSeconds (typeSpeed);
			
		}
		theText.text = lineOfText;
		isTyping = false;
		cancelTyping = false;
	}

	public void EnableTextBox (){
		textBox.SetActive (true);
		isActive = true;
		GameManager.sharedInstance.DisablePlayers ();
		GameManager.sharedInstance.canPause = false;
		StartCoroutine (TextScroll (textlines [currentLine]));

	}

	public void DisableTextBox (){
		textBox.SetActive (false);
		isActive = false;
		GameManager.sharedInstance.EnablePlayers ();
		GameManager.sharedInstance.canPause = true;

	}

	public void ReloadScript(TextAsset theText){
		if (theText != null) {
			endAtLine = textlines.Length;
			textlines = new string[1];
			textlines = theText.text.Split ('\n');
		}
	}
}

