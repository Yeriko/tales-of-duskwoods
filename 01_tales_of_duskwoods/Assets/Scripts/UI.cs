﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/* Orden correcto de la UI

0-Contenedor de los textos de la UI
		0.0-Texto sombra
		0.1-Texto luz
1-Contenedor de la barra inferior
		1.0-Contenedor de la vida			1.1 Contenedor del Stamina
				1.0.0-Barra Borde			1.1.0 Barra Borde
				1.0.1-Barra Damage			1.1.1 Barra Damage
				1.0.2-Barra Vida			1.1.2 Barra Stamina
*/

public class UI : MonoBehaviour {
	Animator anim;
	Vector3 porcentajeBarraHeatlh; //Almacena el espacio de la barra de vida
	Vector3 porcentajeBarraStamina;//Almacena el porcentaje actual de la barra stamina
	float maxWidhtBar;	 //Ayuda a calcular la formula para poder sacar un porcentaje de la barra
	Animator animParent;
	public float timer;




	// Use this for initialization
	void Start(){
		animParent = transform.GetComponentInParent<Animator>();
		timer = 0;
		porcentajeBarraHeatlh = transform.GetChild (1).GetChild (0).GetChild (2).localScale;
		porcentajeBarraStamina = transform.GetChild (1).GetChild (1).GetChild (2).localScale;
		maxWidhtBar = transform.GetChild (1).GetChild (0).GetChild (2).localScale.x;
		anim = transform.GetChild(0).GetComponent<Animator>(); //Recupera el animador del contenedor de los textos de la GUI
	}

	void Update ()
	{
		HealthUpdate ();
		StaminaUpdate ();

	}


	public IEnumerator ShowArea(string name){
		anim.Play ("AreaShow");
		transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = name;//Texto sombra
		transform.GetChild (0).GetChild(1).GetComponent<Text> ().text = name; //Texto real
		yield return new WaitForSeconds (1f);
		anim.Play ("AreaFadeOut");
	}

	void HealthUpdate(){
		if (transform.parent.tag == "Ace") {
			porcentajeBarraHeatlh.x = ((GameManager.sharedInstance.aceHp) * maxWidhtBar) / GameManager.sharedInstance.aceMaxHp;
			transform.GetChild (1).GetChild (0).GetChild (2).localScale = porcentajeBarraHeatlh; //Barra de vida
		}
		if (transform.parent.tag == "Jolyne")
			porcentajeBarraHeatlh.x = ((GameManager.sharedInstance.jolyneHp) * maxWidhtBar) / GameManager.sharedInstance.aceMaxHp;
			transform.GetChild (1).GetChild (0).GetChild (2).localScale = porcentajeBarraHeatlh; //Barra de vida
	}

	void StaminaUpdate (){
		porcentajeBarraStamina.x = (timer * maxWidhtBar) / 1;
		if (porcentajeBarraStamina.x >= maxWidhtBar) {
			porcentajeBarraStamina.x = maxWidhtBar;
		}
		transform.GetChild (1).GetChild (1).GetChild (2).localScale = porcentajeBarraStamina; //Barra de vida
		if (Input.GetButton ("Attack")  ){
			timer += Time.deltaTime;
		}
		if (Input.GetButtonUp ("Attack")  ) {
			timer = 0;
		}
		//Debug.Log (porcentajeBarraStamina.x);

	}
}



