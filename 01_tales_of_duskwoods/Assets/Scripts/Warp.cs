﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Warp : MonoBehaviour {
	public GameObject target;
	public GameObject targetMap; //Variable que indica a que mapa vamos
	bool startTransition = false; //Variable que indica el inicio de la trancision
	bool isFadeIn = false ; //Variable para controlar si es entrada o salida
	float alpha = 0; //Opacidad inicial
	static float  fadeTime = 1f; //Tiempo de la trancision

	GameObject [] areas;

	void Awake (){
		Assert.IsNotNull (target);

		GetComponent<SpriteRenderer> ().enabled = false;
		transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = false;

		Assert.IsNotNull (targetMap);
		areas = GameObject.FindGameObjectsWithTag ("Area");
	}

	IEnumerator OnTriggerEnter2D (Collider2D other)	{
		if (other.GetComponent<Animator> () != null) {
			if (other.tag == "Ace" && GameManager.sharedInstance.aceActive == true) {
				other.GetComponent<Animator> ().enabled = false;
				other.GetComponent<Ace_Behaviour> ().enabled = false;
				FadeIn ();
			} else if (other.tag == "Jolyne" && GameManager.sharedInstance.jolyneActive == true) {
				other.GetComponent<Animator> ().enabled = false;
				other.GetComponent<Jolyne_Bejaviour> ().enabled = false;			
				FadeIn ();
			}

			yield return new WaitForSeconds (fadeTime);
			if (other.tag == "Ace" && GameManager.sharedInstance.aceActive == true) {
				other.transform.position = target.transform.GetChild (0).transform.position;
				Camera.main.GetComponent<MainCamera> ().SetBound (targetMap);
				other.GetComponent<Ace_Behaviour> ().enabled = true;
			} else if (other.tag == "Jolyne" && GameManager.sharedInstance.jolyneActive == true) {
				other.transform.position = target.transform.GetChild (0).transform.position;
				other.GetComponent<Jolyne_Bejaviour> ().enabled = true;
				Camera.main.GetComponent<MainCamera> ().SetBound (targetMap);
				Jolyne_Bejaviour.sharedInstance.currentMap = targetMap;

				//GetComponent<CameraPlayer2> ().SendMessage ("SetBound", targetMap);
				//Camera.main.GetComponent<MainCamera> ().SetBound (targetMap);
			} 
			FadeOut ();
			other.GetComponent<Animator> ().enabled = true;

			foreach (GameObject area in areas) {
				StartCoroutine (area.GetComponent<UI> ().ShowArea (targetMap.name));
			}
		}
	}

	void OnGUI(){
		if (!startTransition) //Salimos si la trancision no es requerida
			return;

		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha);

		//Textura temporal para rellenar la pantalla
		Texture2D tex;
		tex = new Texture2D (1, 1);
		tex.SetPixel (0, 0, Color.black);
		tex.Apply ();

		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), tex); //Dibujamos la textura sobre la pantalla

		if (isFadeIn) {
			alpha = Mathf.Lerp (alpha, 1.1f, fadeTime * Time.deltaTime);
		} else {
			alpha = Mathf.Lerp (alpha, -0.1f, fadeTime * Time.deltaTime);
			if (alpha < 0)
				startTransition = false;
		}

	}

	void FadeIn(){
		startTransition = true;
		isFadeIn = true;
	}

	void FadeOut(){
		isFadeIn = false;
	}
}

